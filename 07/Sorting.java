	class Sorting {
    public static void main(String[] args) {
        int[] mass = {1, 0, 2, 9, 3, 8, 4, 7, 5, 6};
        int i = 0;
        while (i < mass.length-1) {
					if (mass[i] < mass[i + 1]) {
						int k = mass[i];
						mass[i] = mass[i + 1];
						mass[i + 1] = k;
						i = 0;
					}
            else {
                i++;
            }
        }
        for (i = 0; i < mass.length; i++) {
            System.out.println(mass[i]);
        }
    }
}
